import numpy as np
import cv2 as cv
from itertools import groupby, tee, islice, product, dropwhile, takewhile
from functools import partial, reduce
from os import path, scandir
import configparser
from . import batterytemplate
import io
from PIL import Image
debugHandler = None
def debugImg(mat, tag):
    if debugHandler:
        return debugHandler(mat, tag)
        
def collapsecontinue(arr):#TODO make it faster
    arr1 = set(arr)
    arr2 = set(map(lambda k:k+1, arr1))
    return list(sorted(arr1 ^ arr2))

def continouspairs(arr):
    collapsed = collapsecontinue(arr)
    i, ii = tee(collapsed, 2)
    return zip(islice(i,0,None,2),islice(ii,1,None,2))

def nzof1d(arr):
    if not arr.size != (1,): raise Exception('nzof1d only accepts 1d array')
    return continouspairs(np.nonzero(arr)[0])

def outlineimgs(mat, th):
    'return same sized outline image h, v'
    if mat.dtype != "uint8":
        raise Exception('detectlines only accepts uint8')
    fmat = mat.astype(int)
    def filt(axis):
        m = np.diff(fmat, 1, axis)
        m = np.abs(m)
        m = np.sum(m, 2)
        m = np.clip(m,0,255).astype("uint8")
        r, m = cv.threshold(m, int(255.0*th), 255, cv.THRESH_BINARY)
        if not r: raise Exception('thres fail')
        return m
    
    h = filt(0)
    v = filt(1)
    h = h[:,:v.shape[1]]
    v = v[:h.shape[0]]
    return h, v

def hlinesof(m, minlen):
    'returns iterable of pair of (row, continous columns), m must be binary image'
    nzs = np.nonzero(m)#nonzero always return in row-major order.
    grouped = groupby(range(nzs[0].size), nzs[0].__getitem__)
    def pairsof(rcis):
        r, cis = rcis
        cps = continouspairs(map(nzs[1].__getitem__, cis))
        return r, list(filter(lambda p: minlen < p[1]-p[0], cps))
    return filter(lambda t:t[1], map(pairsof, grouped))

def boxof(lines, mingap, maxgap, minlen):
    'lines must be ordered in row. must be outcome of hlinesof function yields xyxy'
    lines = list(lines)
    def gaptest():
        for i in range(len(lines)):
            for j in range(i+1, len(lines)):
                if lines[j][0] < lines[i][0]:
                    raise Exception('wat?')
                gap = lines[j][0] - lines[i][0]
                if not mingap <= gap:
                    continue
                if not gap <= maxgap:
                    break
                yield lines[i], lines[j]
    
    for x, xx in gaptest():
        try:
            i = iter(x[1])
            ii = iter(xx[1])
            s,e = next(i)
            ss,ee = next(ii)
            while True:
                maxstart = max(s,ss)
                minend = min(e,ee)
                if minlen <= minend - maxstart:
                    yield maxstart, x[0], minend, xx[0]
                if e < ee:
                    s, e = next(i)
                elif ee < e:
                    ss, ee = next(ii)
                else:
                    s,e = next(i)
                    ss,ee = next(ii)
        except StopIteration:
            pass

def cupsof(boxs, vlines):
    vlines = list(vlines)
    for xyxy in boxs:
        gap = xyxy[3] - xyxy[1]
        pad = gap//4
        for c, intervals in vlines:
            if xyxy[0]-pad <= c <= xyxy[2]-gap*1.5:
                for start, end in intervals: 
                    if start-pad <= xyxy[1] < xyxy[3] <=end+pad:
                        yield xyxy[1], c, xyxy[3]

def normedcupshot(mat, cup, normh):
    t,l,b = cup[0]+1, cup[1]+1,cup[2]+1
    h = b-t
    sub = mat[t:b,l:int(l+2.5*h)]
    orih, oriw, _ = sub.shape
    return cv.resize(sub, (oriw*normh//orih, normh), interpolation=cv.INTER_AREA)

def mirroredoutline(mat, th):
    outh, outv = outlineimgs(mat, th)
    out = np.dstack((outh, outv))
    height = out.shape[0]
    up = out[:height//2]
    down = np.flipud( out[(height+1)//2:])
    return np.bitwise_and(up,down)

def patternmatch(pattern, img):
    assert pattern.shape == img.shape
    bit = np.bitwise_and(pattern,img)
    return np.array_equal(bit, pattern)

def headmatch(outlinemirror, lv, lh):
    'lv, lh are respectivly vertical, horizontal head outline img'
    lvh = np.dstack((lh, lv))
    
    assert(lvh.shape[0] == outlinemirror.shape[0])
    head = outlinemirror[:,:lvh.shape[1]]
    return patternmatch(lvh, head)

def tipmatch(outlinemirror, rv, rh):
    rvh = np.dstack((rh, rv))
    assert(rvh.shape[0] == outlinemirror.shape[0])
    def isend(x):
        return patternmatch(rvh, outlinemirror[:,x-rvh.shape[1]:x])
    return filter(isend, range(outlinemirror.shape[0]*4, outlinemirror.shape[1]))

def nwise(n, iterable):
    its = tee(iterable, n)
    for i in range(len(its)):
        for _ in range(i):
            next(its[i], None)
    return zip(*its)

def spotsubstanceios11(battery):
    mirror = mirroredoutline(battery, 0.3)
    himg = mirror[:,:,0]
    vimg = mirror[:,:,1]
    #hlines = hlinesof(himg, 3)
    vlines = hlinesof(vimg.T, mirror.shape[0]/2.5)
    def onlylast(vline):
        return vline[0], vline[1][-1]
    vlasts = map(onlylast, vlines)
    def filtend(vlast):
        return vlast[1][1] == mirror.shape[0]
    vends = filter(filtend, vlasts)
    vpairs = nwise(2, sorted(vends, key=lambda l:l[0])) #sorted by column

    hlines = hlinesof(himg, 1)
    def filtY(hline):
        return hline[0] < mirror.shape[0]/2
    htops = list(filter(filtY, hlines))

    pad = mirror.shape[0]//5 #specific to ios11 keep in mind
    def tops():
        for tp in htops:
            for (x,xx) in tp[1]:
                yield tp[0], x-pad, xx+pad
    def walls():
        for (c,(r,rr)), (x,(y,yy)) in vpairs:
            if pad < x-c:
                yield (c,r-pad), (x,y-pad)
    
    def doms():
        for (r,c,cc), ((x,y),(xx,yy)) in product(tops(), walls()):
            if c <= x < xx <= cc:
                if max(y,yy) <= r:
                    yield r, x, xx

    def xyxys():
        for r,x,xx in doms():
            yield x+1,r+1, xx+1, battery.shape[0]-(r+1)

    return xyxys()

def issidestretch(mat, xyxy):
    x,y,xx,yy = xyxy
    l = mat[y:yy,x-1]
    r = mat[y:yy,xx]
    mean = np.mean(np.abs(l.astype(int)-r.astype(int)))
    return mean < 1
    
def isupdownstretch(mat, xyxy):
    x,y,xx,yy = xyxy
    u = mat[y-1,x:xx]
    d = mat[yy,x:xx]
    mean = np.mean(np.abs(u.astype(int)-d.astype(int)))
    return mean < 1

def inpaintbystretch(mat, xyxy):
    x,y,xx,yy = xyxy
    inpaint = np.zeros((yy-y,xx-x,3), 'uint8')
    for r in range(y,yy):
        for d in range(3):
            p = mat[r,x-1,d]
            pp = mat[r,xx,d]
            inpaint[r-y,:,d] = np.interp(range(x,xx), [x-1,xx], [p,pp]).astype('uint8')
    return inpaint

def maybealpha(inpaint, original, color):
    o = np.sum((original.astype(int)-color)**2, 2)
    i = np.sum((inpaint.astype(int)-color)**2, 2)
    r = np.sqrt( 255*255*o // i )
    return np.clip(r,0,255).astype("uint8")

def blend255(mask, a ,b):
    mask = np.dstack((mask,mask,mask)).astype(int)
    return ((mask*a + (255-mask)*b)//255).astype("uint8")

def redden(original, inpaint):
    assert original.shape == inpaint.shape
    bc = original[original.shape[0]//2, original.shape[1]//2]
    alpha = maybealpha(inpaint, original, bc)
    return blend255(alpha, inpaint, np.array([48, 59, 255]))

class PercentNotFound(Exception):
    pass

def percentthres(mat,brightness,coef):
    dark = brightness < 128
    rs,thres = cv.threshold(mat,
                            brightness+coef if dark else brightness-coef,
                            255,
                            cv.THRESH_BINARY_INV if dark else cv.THRESH_BINARY)
    if not rs:raise Exception('thres error')
    return thres

def _spotpercentwithstatusbrightness(colored, gray, statusbrightness, percentimg, digitimgs):
    '''returns (color, num), bounds, map(blur, bounds))'''
    assert len(digitimgs) == 10
    thres = percentthres(gray, statusbrightness, 20)
    thres[:,0] = 255
    def rs(c1,c2):
        _nzs = np.nonzero(thres[:,c1:c2])[0]
        return _nzs[0], _nzs[-1]+1
    
    continous = reversed(list(continouspairs(np.nonzero(np.transpose(thres))[0])))

    def notpercent(interval):
        c1, c2 = interval
        #14 from iphone 5s, 35 from iphone x. loosely
        if not 14<c2-c1<35: return True
        r1,r2 = rs(c1,c2)
        if not 14<r2-r1<35: return True
        th = cv.resize(thres[r1:r2,c1:c2], (percentimg.shape[1], percentimg.shape[0]))
        return cv.norm(th, percentimg, normType=cv.NORM_L1) > 27488

    lst = list(dropwhile(notpercent, continous))

    def detectnum(tights):#tights must be starting with %.
        def istogether(together):
            (b,bb),(a,aa) = together
            q,qq = rs(b,bb)
            return (b-aa) < (qq-q)/2 and (b-aa) < (qq-q)/2
            
        def numof(digitpair):
            '(color, num)'
            x, xx = digitpair[1]
            y, yy = rs(x, xx)
            rz = cv.resize(thres[y:yy,x:xx],(13,20))

            _,_,minloc,maxloc = cv.minMaxLoc(gray[y:yy,x:xx], thres[y:yy,x:xx])
            c,r = minloc if statusbrightness < 128 else maxloc
            return (colored[y:yy,x:xx])[r,c], min(range(10), key=lambda n:cv.norm(rz, digitimgs[n],cv.NORM_L1))

        return map(numof, takewhile(istogether, nwise(2, tights)))

    def maxbound(t):
        (b,_),_,(_,a) = t
        return (a,b)
    
    bounds = list(map(maxbound, nwise(3,lst)))
    
    def blur(bound):#wont be called for all icons bc detectnum() must exhaust before.
        #print("blur called! (shldnt be called more than 3 times")
        x,xx = bound
        blurred = cv.blur(thres[:,x:xx],(3,3))
        rs, blurth = cv.threshold(blurred, 1,255,cv.THRESH_BINARY)
        if not rs: raise Exception('thres fail')
        return blurth

    return zip(detectnum(lst), bounds, map(blur, bounds))

def spotpercent(mat, percentimg, digitimgs):
    '''mat must be containing all digits and percent as tight as possible
    returns (color, num), bounds, bound size blur'''
    gray = cv.cvtColor(mat, cv.COLOR_BGR2GRAY)
    nv, xv, _, _ = cv.minMaxLoc(gray)
    for brightness in [nv,xv]:
        try:
            digitdatas = _spotpercentwithstatusbrightness(mat, gray, brightness, percentimg, digitimgs)
            digitdatas = list(digitdatas)
            if len(digitdatas) > 1:
                return digitdatas
        except PercentNotFound:
            pass
    raise PercentNotFound()

def fillinterp(mat, mask):
    cp = mat.copy()
    for row, lines in hlinesof(mask,1):
        for x, xx in lines:
            for d in range(3):
                p = mat[row,x-1,d]
                pp = mat[row,xx,d]
                line = np.interp(range(x,xx), [x-1,xx], [p,pp]).astype('uint8')
                cp[row,x:xx,d] = line
    return cp

def onepercent(mat, xyxy, digits):
    '''
    digits contains only x bounds relative to xyxy
    digits must be [(color, num), bound, blur]'''
    mask = np.zeros(mat.shape[:2], "uint8")
    digits = list(digits)
    x,y,xx,yy = xyxy
    lbound = xx
    rbound = x
    for digit in digits:
        c,cc = digit[1]
        c += x
        cc += x
        mask[y:yy,c:cc] += digit[2]
        rbound = max(rbound, x+digit[1][1])
        lbound = min(lbound, x+digit[1][0])
    if isupdownstretch(mat, (lbound+1,y+1,rbound-1,yy-1)):
        matsub = mat[y:yy,x:xx]
        masksub = mask[y:yy,x:xx]
        matsub = np.transpose(matsub, (1,0,2))
        masksub = masksub.T
        inpaint = fillinterp(matsub, masksub)
        inpaint = np.transpose(inpaint, (1,0,2))
    else:
        inpaint = cv.inpaint(mat, mask, yy-y, cv.INPAINT_NS)
        inpaint = inpaint[y:yy,x:xx]
    
    digit = min(digits, key=lambda d:d[0][1] if d[0][1] > 0 else 9)
    c,cc = digit[1]
    alpha = maybealpha(inpaint[:,c:cc], mat[y:yy,x+c:x+cc], digit[0][0])
    alpha = np.bitwise_or(alpha, np.bitwise_not( digit[2]))
    rightmargin = digits[0][1][1]
    leftmargin = rightmargin - (cc-c)
    inpaint[:,leftmargin:rightmargin] = blend255(alpha, inpaint[:,leftmargin:rightmargin], digit[0][0])
    return inpaint
    #print('blended inpaint yield!')
    #yield inpaint

def xyby(*xyxys):
    def reducer(q, qq):
        x,y,_,_ = q
        c,r,cc,rr = qq
        return (c+x,r+y,cc+x,rr+y)
    return reduce(reducer, xyxys)
    
def imgfrombytes(imgbytes):
    return Image.open( io.BytesIO(imgbytes))
def mattobytes(mat):
    result, bts = cv.imencode(".png", mat)
    if not result:
        raise Exception('bytes encoding fail.')
    return bts
def mattoimg(mat):
    return imgfrombytes(mattobytes(mat))
def bytestomat(fullimgbytes):
    fullimgnp = np.fromstring(fullimgbytes, dtype='uint8')
    fullimgmat = cv.imdecode(fullimgnp, cv.IMREAD_COLOR)
    fullimgmat.flags.writeable = False
    return fullimgmat
    