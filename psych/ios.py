import numpy as np
import cv2 as cv
from . import core

def spotsubstance(mat):
    mirror = core.mirroredoutline(mat)
    hori = mirror[:,:,0]
    verti = mirror[:,:,1]
    core.hlinesof()