from abc import ABC, abstractproperty, abstractmethod
from os import path
import cv2 as cv
from . import core
import numpy as np
from PIL import Image

class BTemp(ABC):
    @abstractproperty
    def height(self):
        pass
    @abstractproperty
    def minlen(self):
        pass
    @abstractproperty
    def mingap(self):
        pass
    @abstractproperty
    def maxgap(self):
        pass
    
    def homepath(self):
        templatespath = path.join(path.dirname(__file__), 'templates')
        return path.join(templatespath, self.__class__.__name__)
    def rsc(self, name):
        rscpath = path.join(self.homepath(), name)
        assert path.exists(rscpath)
        return rscpath
        
    def __init__(self):
        home = self.homepath()
        self.lv = cv.imread(self.rsc('lv.png'), cv.IMREAD_GRAYSCALE)
        self.lh = cv.imread(self.rsc('lh.png'), cv.IMREAD_GRAYSCALE)
        self.rv = cv.imread(self.rsc('rv.png'), cv.IMREAD_GRAYSCALE)
        self.rh = cv.imread(self.rsc('rh.png'), cv.IMREAD_GRAYSCALE)
    
    def spotbattery(self, mat):
        outh, outv = core.outlineimgs(mat, 0.15)
        lines = core.hlinesof(outh, self.minlen)
        boxs = core.boxof(lines, self.mingap, self.maxgap, self.minlen)
        vlines = core.hlinesof(outv.T, self.mingap*0.6)
        for cup in core.cupsof(boxs, vlines):
            normed = core.normedcupshot(mat, cup, self.height)
            mirrored = core.mirroredoutline(normed, 0.15)
            if core.headmatch(mirrored, self.lv, self.lh):
                tips = core.tipmatch(mirrored, self.rv, self.rh)
                try:
                    firsttip = next(tips)
                    firsttip = firsttip*(cup[2]-cup[0])//self.height
                    yield cup[1]+1, cup[0]+1, cup[1]+firsttip, cup[2]+1
                except StopIteration:
                    pass
    @abstractmethod
    def do(self, mat):
        pass

class ios11(BTemp):
    height = 23
    mingap = 18
    maxgap = 35
    minlen = 38
    def __init__(self):
        super().__init__()
        self.percentimg = cv.imread(self.rsc("%.png"), cv.IMREAD_GRAYSCALE)
        self.percentimg.flags.writeable = False
        def char2img(c):
            img = cv.resize(cv.imread(self.rsc(f"{c}.png"), cv.IMREAD_GRAYSCALE),(13,20))
            img.flags.writeable = False
            return img
        self.digitimgs = list(map(char2img, '0123456789'))
        
    def guesspercentrect(self, mat, xyxy):
        '''xyxy is battery rect'''
        if not (xyxy[0] < xyxy[2] and  xyxy[1] < xyxy[3]): raise Exception(f"wrong xyxy. {xyxy}")
        xx = xyxy[0] - 3 #from iphone 5s
        x = xx - 125 #from iphone x
        y = xyxy[1]
        yy = xyxy[3]
        return (x,y,xx,yy)
        
    def do(self, pngfile):
        mat = core.bytestomat(pngfile.read())
        subxyxy = (mat.shape[1]-290, 0, mat.shape[1], 320)
        x,y,xx,yy = subxyxy
        sub = mat[y:yy,x:xx]
        batteryspotter = self.spotbattery(sub)
        batteryxyxy = next(batteryspotter)
        x,y,xx,yy = batteryxyxy
        battery = sub[y:yy,x:xx]
        substancespotter = core.spotsubstanceios11(battery)
        substancexyxy = next(substancespotter)
        x,y,xx,yy = substancexyxy
        
        if core.issidestretch(battery, substancexyxy):
            inpaint = core.inpaintbystretch(battery, substancexyxy)
        elif core.isupdownstretch(battery, substancexyxy):
            inpaint = core.inpaintbystretch(np.transpose(battery, (1,0,2)), (y,x,yy,xx))
            inpaint = np.transpose(inpaint,(1,0,2))
        else:
            mask = np.zeros(sub.shape[:2], 'uint8')
            c,r,cc,rr = batteryxyxy[0]+x, batteryxyxy[1]+y, batteryxyxy[0]+xx, batteryxyxy[1]+yy
            mask[r:rr,c:cc] = 255
            inpainted = cv.inpaint(sub,mask,yy-y,cv.INPAINT_NS)
            inpaint = inpainted[r:rr,c:cc]
        
        redden = core.redden(battery[y:yy,x:xx], inpaint)
        pad = (batteryxyxy[3]-batteryxyxy[1])//10
        redden[:,pad:] = inpaint[:,pad:]
        
        reserves = []
        reserves.append((core.xyby(subxyxy, batteryxyxy, substancexyxy), redden))

        try:
            percentxyxy = self.guesspercentrect(sub, batteryxyxy)
            x,y,xx,yy = percentxyxy
            digits = core.spotpercent(sub[y:yy,x:xx], self.percentimg, self.digitimgs)
            inpaint = core.onepercent(sub, percentxyxy, digits)
            reserves.append((core.xyby(subxyxy, percentxyxy), inpaint))
        except core.PercentNotFound:
            pass

        pngfile.seek(0)
        png = Image.open(pngfile)
        
        for xyxy, box in reserves:
            png.paste(core.mattoimg(box), xyxy)

        return png
