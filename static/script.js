function waiter(n) {
    $(".loading").html("Loading (Might take a while) "+Array((n%3)+2).join("."));
    setTimeout(function(){
        waiter(n+1)
    }, 400)
}

$( document ).ready(function(){
    waiter(0)
    $('#file').change(function(efile) {
        $('.loading').removeClass('hidden')
        $('#response').addClass('hidden')
        var options = { 
            target: $('#response'),       //document.body,   // target element(s) to be updated with server response 
            complete: function(jqXHR, textStatus) {
                $('.loading').addClass('hidden')
                $('#response').removeClass('hidden')
            }
        };
        $('#form').ajaxSubmit(options)
    })
})