export FLASK_APP=main.py
#flask run
if [ "$1" = "d" ]
then
  export FLASK_DEBUG=1
  flask run
else
  flask run --host=0.0.0.0
fi