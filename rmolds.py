#!/usr/bin/python3.6
import os
import shutil
import time

with os.scandir(os.path.join(os.path.dirname(__file__), 'static')) as scans:
    def filt(d):
        if d.is_file():
            if os.path.splitext( d.name)[-1] == '.png':
                return 60 < time.time() -  d.stat().st_ctime
        return False
    dirs = filter(filt, scans)
    paths = map(lambda d:d.path, dirs)

    for p in paths:
        os.remove(p)