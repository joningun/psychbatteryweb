from flask import Flask, request, redirect, url_for, render_template, abort, Response, g, make_response, send_file
from werkzeug.utils import secure_filename
import os
from os import path
import base64
import secrets
import io
from PIL import Image
import shutil
import cv2 as cv
import numpy as np
import tempfile
from itertools import count, repeat
from functools import reduce
from psych import batterytemplate
app = Flask(__name__)
# limit maximum size of file upload 10mb
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

ios11 = batterytemplate.ios11()
@app.route('/sc', methods=['POST'])
def sc():
    if 'file' in request.files:
        screenshotfile = request.files['file']
    else:
        return render_template('noimg.html', homelink=url_for('home'), msg='No image found.')

    if os.path.splitext(screenshotfile.filename)[-1].lower() != '.png':
        msgs = [
            'Your screenshot is either too old, or taken from other device.',
            'Please try newly taken screenshot.'
        ]
        return render_template('sorry.html', msgs=msgs)

    platform = request.form['platform']

    if platform != 'ios':
        return redirect(url_for('noandroidyet'))
    try:
        final = ios11.do(screenshotfile)

        scid = secrets.token_urlsafe(16)
        savepath = os.path.join(app.root_path, app.static_folder, f'{scid}.png')
        with open(savepath, 'wb') as fobj:
            final.save(fobj, "PNG")
        return render_template('imgview.html', src=url_for('static',filename=f'{scid}.png'))
    except:
        return render_template('sorry.html', homelink=url_for('home'))
        
@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.errorhandler(500)
def gimmesc(e):
    return 'Would you please try a different, recent screenshot?', 500
